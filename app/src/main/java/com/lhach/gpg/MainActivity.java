package com.lhach.gpg;

import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.appcompat.app.AppCompatActivity;

import com.github.slashrootv200.retrofithtmlconverter.HtmlConverterFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.jsoup.nodes.Document;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    String link = "https://gtpay-preprod.prosperus.tech:4003/exchanger/form";

    WebView webView;
    Retrofit retrofit;
    private GpgService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        webView = new WebView(this);

        setContentView(webView);

        setup();
    }

    private void setup() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl("https://gtpay-preprod.prosperus.tech:4003")
                //.addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(HtmlConverterFactory.create("https://gtpay-preprod.prosperus.tech:4003"))
                //.addConverterFactory(PageAdapter.FACTORY)
                .build();

        service = retrofit.create(GpgService.class);

        WebViewClientImpl webViewClient = new WebViewClientImpl(this);
        webView.setWebViewClient(webViewClient);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.getSettings().setUseWideViewPort(false);

        String obj = "{\"amount\":5000,\"nym\":\"CucgwgFiHRSF2gEd18iSQLBCmyVYkMQS8XTZbU6RvtYN\",\"data\":{\"email\":\"anas.mohamed@digitus.tech\",\"city\":\"Tunis\",\"country\":\"TN\",\"cardNumber\":\"5104051111111115\",\"telephone\":\"50505505\",\"custAddress\":\"address\",\"validityMonth\":12,\"validityYear\":2020,\"ccv\":\"004\",\"firstName\":\"Wael\",\"lastName\":\"BenDhia\"}}";
        CashinWithWebViewRequest b = new CashinWithWebViewRequest(new CreditCardInfo(), 1000, "CucgwgFiHRSF2gEd18iSQLBCmyVYkMQS8XTZbU6RvtYN");
        Call<Document> gpgPage = service.getGpgPage(b);

        gpgPage.enqueue(new Callback<Document>() {
            @Override
            public void onResponse(Call<Document> call, Response<Document> response) {
                //webView.loadData(response.toString(), "text/html; charset=UTF-8", "UTF-8");
            }

            @Override
            public void onFailure(Call<Document> call, Throwable t) {
                Log.e("error", "gpg", t);
            }
        });
    }
}
