package com.lhach.gpg;

import org.jsoup.nodes.Document;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface GpgService {
    @Headers("Content-Type: application/json")
    @POST("/exchanger/form")
    Call<Document> getGpgPage(@Body CashinWithWebViewRequest b);
}
