package com.lhach.gpg;
import androidx.annotation.NonNull;

/**
 * credit card information
 */
public class CreditCardInfo {

    private String lastName;
    private String firstName;
    private String telephone;
    private String email;
    private String city;
    private String country;
    private String cardNumber;
    private int validityMonth;
    private int validityYear;
    private String cvv;
    @NonNull
    private String port = "GPG";

    /**
     *
     */
    public CreditCardInfo() {
        this("Mohamed",
                "Anas",
                "56540153",
                "essalah.elhechmi@gmail.com",
                "Tunis",
                "TN",
                "5471251111111116",
                12,
                2020,
                "524");
    }

    /**
     * @param lastName
     * @param firstName
     * @param telephone
     * @param email
     * @param city
     * @param country
     * @param cardNumber
     * @param validityMonth
     * @param validityYear
     * @param cvv
     */
    public CreditCardInfo(String lastName,
                          String firstName,
                          String telephone,
                          String email,
                          String city,
                          String country,
                          String cardNumber,
                          int validityMonth,
                          int validityYear,
                          String cvv) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.telephone = telephone;
        this.email = email;
        this.city = city;
        this.country = country;
        this.cardNumber = cardNumber;
        this.validityMonth = validityMonth;
        this.validityYear = validityYear;
        this.cvv = cvv;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getValidityMonth() {
        return validityMonth;
    }

    public void setValidityMonth(int validityMonth) {
        this.validityMonth = validityMonth;
    }

    public int getValidityYear() {
        return validityYear;
    }

    public void setValidityYear(int validityYear) {
        this.validityYear = validityYear;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }
}
