package com.lhach.gpg;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

public class PageAdapter implements Converter<ResponseBody, Page> {
    static final Converter.Factory FACTORY = new Converter.Factory() {
        @Override
        public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
            if (type == Page.class) return new PageAdapter();
            return null;
        }
    };

    @Override
    public Page convert(ResponseBody responseBody) throws IOException {
        Document document = Jsoup.parse(responseBody.string());
        Element value = document.select("script").get(1);
        String content = value.html();
        return new Page(document.wholeText());
    }
}
