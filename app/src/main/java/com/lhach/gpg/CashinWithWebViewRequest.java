package com.lhach.gpg;

public class CashinWithWebViewRequest {
    private CreditCardInfo data;
    private  long amount ;
    private  String nym;

    public CashinWithWebViewRequest(CreditCardInfo data, long cashInAmount, String nym) {
        this.data = data;
        this.amount = cashInAmount;
        this.nym = nym;
    }

    public CreditCardInfo getData() {
        return data;
    }

    public void setData(CreditCardInfo data) {
        this.data = data;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getNym() {
        return nym;
    }

    public void setNym(String nym) {
        this.nym = nym;
    }
}
